# Metadata från Azure till Skolfederation

Såhär gör du för att skapa metadata-fil till Skolfederation om du använder Azure som IdP.

1. Gå till https://portal.azure.com. 

2. I vänstermenyn väljer du Azure Active Directory och sedan Enterprise Applications.

3. Klicka på New Application och sedan Create your own application

4. Ge applikationen ett namn och välj non-gallery

5. Klicka på set up single sign on

6. Välj SAML

7. Under Basic SAML Configuration gör du följande:

- Under Entity ID behöver du lägga till alla SP som skolan vill erbjuda inloggning till genom Skolfederation. Här har jag lagt in SP för digitala nationella prov.

https://skolverket.eduid.se/dnp/sp/



- Under Reply URL behöver du lägga till svars-URL för alla SP som skolan vill erbjuda inloggning till genom Skolfederation. Följande är motsvarande svars-URL som jag lagt till för digitala nationella prov:

https://skolverket.eduid.se/Saml2SP/acs/post


Spara dina inställningar och gå tillbaka till sidan med inställningar för SAML SSO.

8. Under Attributes and Claims lägger du till uppgifter om användarna som tjänster ska kunna ta del av via Skolfederation. Jag har bara lagt in eduPersonPrincipalName.

- Under Required claim lägger du in ett claim med Claim Name _Unique User Identifier (Name ID)_, Name identifier format _Persistent_, och Source attribute _user.userprincipalname_.

- Under Additional claims lägger du in ett claim med Claim name _urn:oid:1.3.6.1.4.1.5923.1.1.1.6_ och Source Transformation Join(user.objectid, "@", *din domän*) 

![Screenshot av inställningarna](source_transformation.png)

    **OBS!**: Av någon anledning kommuniceras inte claims korrekt med just Skolverket med den här inställningen. Workaround är istället välja Claim name _eduPersonPrincipalName_.

    **Varför transformation?**: Vi vill inte skicka identifierande uppgifter i onödan. Därför använder vi det för Azure interna objekt-ID istället för användarnamnet vilket ofta innehåller användarens namn eller andra personuppgifter.

9. Under SAML Certificates klickar du på att ladda ner Federation Metadata XML

10. Placera filen du laddade ned i samma mapp som detta script.

11. Kör scriptet app.py med python. (Först behöver du installera python och paketet bs4)

12. Säg ja till att konfigurera config-filen.

13. Svara på frågorna.

14. Ladda upp metadata.xml till Skolfederation när programmet är klart.
