from bs4 import BeautifulSoup as bs
from string import Template
import configparser

config = configparser.ConfigParser()
config.optionxform = str


def extract_xml_from_file(xmlfile:str) -> bs:
    with open(xmlfile,'r') as f:
        b = bs(f.read(),'xml')
    return b


def extract_metadata_from_azure_xml(xml:bs) -> dict[str,str]:
    return {
        "ID": xml.find('EntityDescriptor')['ID'],
        'entityID': xml.find('EntityDescriptor')['entityID'],
        'URI': xml.find('Reference')['URI'],
        'DigestValue': xml.find('DigestValue').text,
        'SignatureValue': xml.find('SignatureValue').text,
        'X509Certificate': xml.find('X509Certificate').text,
        'Location': xml.find('SingleSignOnService')['Location']
    }


def write_config():
    Scope = input('Scope')
    OrganizationNameSv = input('OrganizationNameSv')
    OrganizationNameEn = input('OrganizationNameEn')
    OrganizationDisplayNameSv = input('OrganizationDisplayNameSv')
    OrganizationDisplayNameEn = input('OrganizationDisplayNameEn')
    OrganizationURLSv = input('OrganizationURLSv')
    OrganizationURLEn = input('OrganizationURLEn')
    SupportEmailSv = input('SupportEmailSv')
    SupportEmailEn = input('SupportEmailEn')
    TechnicalEmailSv = input('TechnicalEmailSv')
    Company = input('Company')
    TechnicalEmailEn = input('TechnicalEmailEn')

    config['OrganizationValues'] = {
        'Scope': Scope,
        'OrganizationNameSv': OrganizationNameSv,
        'OrganizationNameEn': OrganizationNameEn,
        'OrganizationDisplayNameSv': OrganizationDisplayNameSv,
        'OrganizationDisplayNameEn': OrganizationDisplayNameEn,
        'OrganizationURLSv': OrganizationURLSv,
        'OrganizationURLEn': OrganizationURLEn,
        'SupportEmailSv': SupportEmailSv,
        'SupportEmailEn': SupportEmailEn,
        'TechnicalEmailSv': TechnicalEmailSv,
        'Company': Company,
        'TechnicalEmailEn': TechnicalEmailEn
    }

    with open('config.ini', 'w') as configfile:
        config.write(configfile)


def read_config():
    config.read('config.ini')
    return dict(config['OrganizationValues'])


def write_new_metadata():
    azurefile = input('Type the filename of the metadata file from Azure')
    azure = extract_metadata_from_azure_xml(extract_xml_from_file(azurefile))

    if input("Do you need to rewrite the config file? [y/N]") == 'y':
        write_config()
    organization = read_config()

    with open('template.xml','r') as f:
        x = f.read()
    t = Template(x)

    r = t.substitute(**azure,**organization)

    with open('metadata.xml','w') as f:
        f.write(r)

write_new_metadata()
